<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Personnage;
use App\Models\Image;


class PersonnageController extends Controller
{
    public function index(){
        return view('welcome',['personnages' => Personnage::all(),'images' => Image::all()]);
    }

    public function store(Request $req){
        $personnage = new Personnage();

        if($req->image == "Choisir une image...")
        {
            $req->image = "pasPhoto.png";
        }

        $personnage->nom = $req->nom;
        $personnage->prenom = $req->prenom;
        $personnage->image = $req->image;
        $personnage->description = $req->description;
        $personnage->save();

        return back()->with("success","C'est OK");
    }

    public function update(Request $req){

        $personnage = Personnage::where('id', $req->id);
        if($req->image == "Choisir une image...")
        {
            $req->image = "pasPhoto.png";
        }
        $personnage->update([
            "nom" => $req->nom,
            "prenom" => $req->prenom,
            "image" => $req->image,
            "description" => $req->description,
        ]);

        return back()->with("success","C'est OK");
    }


    public function delete(Request $req)
    {
        $personnage = Personnage::find($req->idStar);
        $personnage->delete();
        return back()->with("success","C'est OK");
    }
}
