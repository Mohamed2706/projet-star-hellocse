import './bootstrap';

//récupération de tout les li contenant tout les prenoms des stars
const onglets = document.querySelectorAll('li');
//récupération de toute les div de star avec leur contenu
const contenu = document.querySelectorAll('.contenu');

let index = 0;

//je parcours tout les onglets pour leur creer un evenement de click chacun
onglets.forEach(onglet => {
    onglet.addEventListener('click', () => {

        //Si l'onglet a deja la classe active 
        if (onglet.classList.contains('active')) {
            return;
        }
        //sinon on lui ajoute celle ci
        else {
            onglet.classList.add('active')
        }

        //on recuperere le contenu de data anim pour recuperer la division correspondante 
        index = onglet.getAttribute('data-anim');

        //on parcoure tout les onglet pour pouvoir comparer notre index au data anim de tout les onglets
        for (let i = 0; i < onglets.length; i++) {
            if (onglets[i].getAttribute('data-anim') != index) {
                //lorsqu'on le trouve on le retire
                onglets[i].classList.remove('active');
            }
        }


        //On parcoure pour ajouter ou retirer la div qui devra être affiché
        for (let j = 0; j < contenu.length; j++) {
            console.log(contenu[j]);
            console.log(index);
            if (contenu[j].getAttribute('data-anim') == index) {
                contenu[j].classList.add('activeContenu');
            }
            else {
                contenu[j].classList.remove('activeContenu');
            }
        }
    })
})







