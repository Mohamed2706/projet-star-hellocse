<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PersonnageController;

//Route menant a la fonction delete (supprimer) du controlleur Personnage
Route::post('/deletePerso', [PersonnageController::class, 'delete'])->name("perso.delete");
//Route menant a la fonction update (modifier) du controlleur Personnage
Route::post('/updatePerso', [PersonnageController::class, 'update'])->name("perso.update");
//Route menant a la fonction store (ajout) du controlleur Personnage
Route::post('/ajoutPerso', [PersonnageController::class, 'store'])->name("perso.store");
//Route menant a la fonction index du controlleur Personnage donc à la page principal
Route::get('/', [PersonnageController::class, 'index']);

