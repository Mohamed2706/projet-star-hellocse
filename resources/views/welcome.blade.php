<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/app.css" />
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
<!-------------------------------------------  PAGE VERSION WEB  --------------------------------------------------------->    

    <div class="divPage">
        <div class="creerTitre">
            <button type="button" class="btn btn-dark btnCreation" data-bs-toggle="modal" data-bs-target="#creerStar">
                Créer une star
            </button>
            <h1 class="titre">Profile Browser</h1>
        </div>
        
        <div class="detailStar">

            <div class="nomStar">
                <ul class="listeNom">
                    @foreach($personnages as $personnage)
                    @if($personnage['id'] == 1)
                        <li class="active" data-anim="{{$personnage['id']}}">
                        {{$personnage['nom'] . " " . $personnage['prenom']}}
                        </li>
                    @else
                        <li class="" data-anim="{{$personnage['id']}}">
                            {{$personnage['nom'] . " " . $personnage['prenom']}}
                        </li>
                    @endif
                    @endforeach
                </ul>
            </div>

            <div class="descriptionStar">
                @foreach($personnages as $personnage)
                    @if($personnage['id'] == 1)
                    <div class="contenu activeContenu" data-anim="{{$personnage['id']}}" style="padding-left: 1rem;position:absolute;width:100%">
                        <img class="imagePersonnage" src="./images/{{$personnage['image']}}" alt="Photo test">
                        <h1>{{$personnage['nom'] . " " . $personnage['prenom']}}</h1>
                        <p>{{$personnage['description']}}</p>
                        <button onclick="modifModal(this)" type="button" class="btn btn-dark">
                            Modifier une star
                        </button>
                        <button onclick="supprModal({{$personnage['id']}})" id="btnSuppr" type="button" class="btn btn-dark">
                            Supprimer une star
                        </button>
                    </div>
                    @else
                    <div class="contenu" data-anim="{{$personnage['id']}}" style="padding-left: 1rem;position:absolute;width:100%">
                        <img class="imagePersonnage" src="./images/{{$personnage['image']}}" alt="Photo test">
                        <h1>{{$personnage['nom'] . " " . $personnage['prenom']}}</h1>
                        <p>{{$personnage['description']}}</p>
                        <button onclick="modifModal(this)" type="button" class="btn btn-dark">
                            Modifier une star
                        </button>
                        <button onclick="supprModal({{$personnage['id']}})" id="btnSuppr" type="button" class="btn btn-dark">
                            Supprimer une star
                        </button>
                    </div>
                    @endif
                @endforeach 
            </div>
        </div>
    </div>


<!-------------------------------------------  PAGE VERSION MOBILE  --------------------------------------------------------->    
<div class="divPage2">
    <div class="creerTitre">
        <button type="button" class="btn btn-dark btnCreation" data-bs-toggle="modal" data-bs-target="#creerStar">
            Créer une star
        </button>
        <h1 class="titre">Profile Browser</h1>
    </div>
    
    <div class="detailStar">

        @foreach($personnages as $personnage)
        <div id="{{$personnage['id']}}" onclick="ouvrirStar(this)" class="nomStar">
            <p style="margin-bottom: 0; margin-left:0.3rem">{{$personnage['nom'] . " " . $personnage['prenom']}}</p>
        </div>

        <div class="descriptionStar descriptionStar{{$personnage['id']}}">
            <h1 style="margin-top: 0.5rem">{{$personnage['nom'] . " " . $personnage['prenom']}}</h1>
            <img class="imagePersonnage" src="./images/{{$personnage['image']}}" alt="Photo test">
            <p style="width: 95%">{{$personnage['description']}}</p>
            <button onclick="modifModalMobile(this)" type="button" class="btn btn-dark">
                Modifier une star
            </button>
            <button onclick="supprModal({{$personnage['id']}})" id="btnSuppr" type="button" class="btn btn-dark">
                Supprimer une star
            </button>
        </div>

        @endforeach
    </div>
</div>


<!-------------------------------------------  MODAL  --------------------------------------------------------->    

    <div class="modal" id="creerStar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form method="POST" action="{{ route('perso.store') }}">
                @csrf
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Creer une star</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label fclass="form-label">Nom</label>
                        <input name="nom" type="text" class="form-control" id="nom">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Prénom</label>
                        <input name="prenom" type="text" class="form-control" id="prenom">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Image</label>
                        <select name="image" class="form-select" aria-label="Default select example">
                            <option selected>Choisir une image...</option>
                            @foreach($images as $image)
                            <option value="{{$image->image}}">{{$image->nom}}</option>
                            @endforeach
                            </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Description</label>
                        <textarea name="description" class="form-control" id="description" rows="6"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-dark">Creer</button>
                </div>
                </div>
            </form>
        </div>
    </div>



    <div class="modal fade" id="modifStar" tabindex="-1" aria-labelledby="ModalModif" aria-hidden="true">
        <div class="modal-dialog">
            <form method="POST" action="{{ route('perso.update') }}">
                @csrf
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalModif">Modifier une star</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input hidden name="id" type="text" id="idStarModif">
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Nom</label>
                        <input type="text" class="form-control" name="nom" id="nomModif">
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Prénom</label>
                        <input type="text" class="form-control" name="prenom" id="prenomModif">
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Image</label>
                        <select id="imageModif" name="image" class="form-select" aria-label="Default select example">
                            <option selected>Choisir une image...</option>
                            @foreach($images as $image)
                            <option value="{{$image->image}}">{{$image->nom}}</option>
                            @endforeach
                            </select>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Description</label>
                        <textarea name="description" class="form-control" id="descriptionModif" rows="6"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-dark">Modifier</button>
                </div>
            </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="supprStar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Supprimer une star</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                Etes vous sûr de vouloir supprimer cette star ?
                </div>
                <div class="modal-footer">
                    <form method="POST" action="{{route("perso.delete")}}">
                        @csrf
                        <input type="hidden" value="" id="idStar" name="idStar">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Non</button>
                        <button id="confirmSuppr" type="submit" class="btn btn-dark">Oui</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


</body>
<script>
    function supprModal(id) {
        //On ajoute l'attribue value avec l'id du personnage qu'on veut supprimer pour qu'il passe dans le formulaire
        document.getElementById("idStar").setAttribute('value', id);
       $('#supprStar').modal("show");
    }

    function modifModal(div)
    {
        //recupération des valeurs actuels de la star lors du clic sur le bouton modifier
       const tabNomPrenom = div.parentNode.childNodes[3].innerHTML.split(' ');
       const prenom = tabNomPrenom[1];
       const nom = tabNomPrenom[0];
       const description = div.parentNode.childNodes[5].innerHTML;
       const id = div.parentNode.getAttribute('data-anim');


       console.log($('#imageModif option[value="'+ prenom +'.jpeg"]'));
       //Modification des inputs pour mettre les bonnes valeurs 
       $('#nomModif').val(nom);
       $('#prenomModif').val(prenom);
       $('#descriptionModif').val(description);
       $('#imageModif option[value="'+ prenom.toLowerCase() +'.jpeg"]').attr("selected",true);
       $('#idStarModif').val(id);

       //On affiche la modal
       $('#modifStar').modal("show");

    }

    function modifModalMobile(div)
    {
        //recupération des valeurs actuels de la star lors du clic sur le bouton modifier
       const tabNomPrenom = div.parentNode.childNodes[1].innerHTML.split(' ');
       const prenom = tabNomPrenom[1];
       const nom = tabNomPrenom[0];
       const description = div.parentNode.childNodes[5].innerHTML;
       const id = div.parentNode.getAttribute('data-anim');

       //Modification des inputs pour mettre les bonnes valeurs 
       $('#nomModif').val(nom);
       $('#prenomModif').val(prenom);
       $('#descriptionModif').val(description);
       $('#imageModif option[value="'+ prenom.toLowerCase() +'.jpeg"]').attr("selected",true);
       $('#idStarModif').val(id);

       //On affiche la modal
       $('#modifStar').modal("show");

    }

    //Pour ouvrir les divisions correspondantes au nom que l'on a cliqué
    function ouvrirStar(bouton) {
        const idDiv = bouton.getAttribute('id');
        const divCorrespondante = ".descriptionStar" + idDiv;

        if($(divCorrespondante).css("display") == "none"){
            $(divCorrespondante).attr('style', 'display: block !important;animation: fade 1s forwards;')
        }
        else{
            $(divCorrespondante).attr('style', 'display: none !important;animation: fade 0.5s forwards;')

        }
    }

</script>
<script src="/js/app.js"></script>
</html>